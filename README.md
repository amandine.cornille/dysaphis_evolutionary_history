-Scripts and inputs for the PCA, splistrees, and ABCRF inferences from Vazquez et al. https://www.biorxiv.org/content/10.1101/2020.12.11.421644v1.abstract

Other analyses were done using user-friendly interfaces (see manuscript for more details).

-Excel file with the genotypes of the 667 aphid individuals for the 30 microsatellite markers used in this study (see manuscript for more details).

-Sequences are released on NCBI.
