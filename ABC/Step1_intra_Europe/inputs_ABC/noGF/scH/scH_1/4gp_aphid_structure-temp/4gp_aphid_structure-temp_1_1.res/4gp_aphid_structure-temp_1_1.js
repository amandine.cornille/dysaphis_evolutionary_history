
USETEXTLINKS = 1
STARTALLOPEN = 0
WRAPTEXT = 1
PRESERVESTATE = 0
HIGHLIGHT = 1
ICONPATH = 'file:////work_home/acornille/ABC_aphid/STRUCTURE_5POP/GF_GR/scH/scH_1/'    //change if the gif's folder is a subfolder, for example: 'images/'

foldersTree = gFld("<i>ARLEQUIN RESULTS (4gp_aphid_structure-temp_1_1.arp)</i>", "")
insDoc(foldersTree, gLnk("R", "Arlequin log file", "Arlequin_log.txt"))
	aux1 = insFld(foldersTree, gFld("Run of 01/11/20 at 09:00:17", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17"))
	insDoc(aux1, gLnk("R", "Settings", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_run_information"))
		aux2 = insFld(aux1, gFld("Shared haplotypes", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_shared%20haplotypes"))
		insDoc(aux2, gLnk("R", "Sample 1", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_gr_shared0"))
		insDoc(aux2, gLnk("R", "Sample 2", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_gr_shared1"))
		insDoc(aux2, gLnk("R", "Sample 3", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_gr_shared2"))
		aux2 = insFld(aux1, gFld("Samples", ""))
		insDoc(aux2, gLnk("R", "Sample 1", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_group0"))
		insDoc(aux2, gLnk("R", "Sample 2", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_group1"))
		insDoc(aux2, gLnk("R", "Sample 3", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_group2"))
		aux2 = insFld(aux1, gFld("Within-samples summary", ""))
		insDoc(aux2, gLnk("R", "Basic indices", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_Basic"))
		insDoc(aux2, gLnk("R", "Heterozygosity", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_het"))
		insDoc(aux2, gLnk("R", "Theta(H)", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_thetaH"))
		insDoc(aux2, gLnk("R", "No. of alleles", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_numAll"))
		insDoc(aux2, gLnk("R", "Allelic range", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_allRange"))
		insDoc(aux2, gLnk("R", "Garza-Williamson index", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_GW"))
		insDoc(aux2, gLnk("R", "Garza-Williamson modified index", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_comp_sum_GWN"))
		aux2 = insFld(aux1, gFld("Genetic structure (samp=pop)", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_17_pop_gen_struct"))
