
USETEXTLINKS = 1
STARTALLOPEN = 0
WRAPTEXT = 1
PRESERVESTATE = 0
HIGHLIGHT = 1
ICONPATH = 'file:////work_home/acornille/ABC_aphid/STRUCTURE_5POP/GF_GR/scK/scK_1/'    //change if the gif's folder is a subfolder, for example: 'images/'

foldersTree = gFld("<i>ARLEQUIN RESULTS (4gp_aphid_structure-temp_1_1.arp)</i>", "")
insDoc(foldersTree, gLnk("R", "Arlequin log file", "Arlequin_log.txt"))
	aux1 = insFld(foldersTree, gFld("Run of 01/11/20 at 09:00:54", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54"))
	insDoc(aux1, gLnk("R", "Settings", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_run_information"))
		aux2 = insFld(aux1, gFld("Shared haplotypes", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_shared%20haplotypes"))
		insDoc(aux2, gLnk("R", "Sample 1", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_gr_shared0"))
		insDoc(aux2, gLnk("R", "Sample 2", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_gr_shared1"))
		insDoc(aux2, gLnk("R", "Sample 3", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_gr_shared2"))
		aux2 = insFld(aux1, gFld("Samples", ""))
		insDoc(aux2, gLnk("R", "Sample 1", "4gp_aphid_structure-temp_1_1.xml#01_11_20at09_00_54_group0"))
