#MERGE ALL THE RESULTS FOR ALL PRIOR SET AND COPY THEM IN THE NEW FOLDER all_results

#mkdir all_results
for j in {A..L}
do
	cd sc$j
	head -n 1 sc$j'_1'/out_sampling1.txt > res_sc$j'_noGF_GR.txt'
	for i in {1..10}
	do
		tail -n +2 sc$j'_'$i/out_sampling1.txt >> res_sc$j'_noGF_GR.txt'
	done
		cp res_sc$j'_noGF_GR.txt' /home/acornille/work/ABC_aphid/STRUCTURE_5POP/RES
	cd ..
done
