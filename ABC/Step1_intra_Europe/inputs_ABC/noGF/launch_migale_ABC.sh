#!/bin/bash

### ce script est un exemple, il est necessaire de le modifier pour l'adapter a votre besoin ###

################################################
# Les commentraires qui commencent par '#$' sont
# interpretes par SGE comme des options en ligne
################################################

# Shell à utiliser pour l'execution du job
#$ -S /bin/bash

# Utilisateur à avertir
#$ -M chrousseblabla@yahoo.fr

# Export de toutes les variables d'environnement
#$ -V

# Avertir au debut (b)egin, a la fin (e)nd, a l'eliminaton (a)bort et
# a la suspension (s)uspend d'un job
#$ -m bea

# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#$ -o /home/acornille/work/ABC_aphid/ABC.out

# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#$ -e /home/acornille/work/ABC_aphid/ABC.err

# Lance la commande depuis le repertoire ou est lance le script
#$ -cwd

#Exemple pour l'utilisation de blast :

./ABCtoolbox 4gp_aphid_structure.input task=simulate

